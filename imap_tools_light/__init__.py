#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# __init__.py
# :author: Markus Killer
# :copyright: September 2013
# :license: GNU GPLv3, see LICENSES for more details.

version_info = (1, 1, 0, 'beta')

# adpated from IMAPClient.__init__
def _version_string(vinfo):
    major, minor, micro, releaselevel = vinfo
    v = '%d.%d' % (major, minor)
    if micro != 0:
        v += '.%d' % micro
    if releaselevel != 'final':
        v += '-' + releaselevel
    return v

 
__version__ = _version_string(version_info)
VERSION = __version__

AUTHOR = "Markus Killer"
AUTHOR_INITIALS = "MKI"
AUTHOR_EMAIL = "<mki5600*AT*gmail*DOT*com>"
__author__ = AUTHOR + " " + AUTHOR_EMAIL
AUTHOR_PLUS_EMAIL = __author__

PROJECT_TITLE = "IMAP Tools Light"
PROJECT_FULL_TITLE = PROJECT_TITLE + " for Python3"
PROJECT_SUBTITLE = "Test, Migrate & De-duplicate IMAP EMAIL ACCOUNTS"
COPYRIGHT_YEARS = "2013"
LICENSE = "GNU GPLv3"
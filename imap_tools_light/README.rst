============================
IMAP Tools Light for Python3
============================

Test, Migrate & De-duplicate IMAP EMAIL ACCOUNTS
------------------------------------------------

:Version: 1.1-beta
:Copyright: 2013 
:License: GNU GPLv3
:Author: Markus Killer <mki5600*AT*gmail*DOT*com>

:Description: Migrate IMAP email messages from source mailbox(es) to destination mailbox(es) with an optional duplicate email filter. There are also some basic IMAP diagnostic tools and an experimental one that removes all duplicate emails in specified IMAP mailbox(es) or across all mailboxes of an account.

:Check for latest version on: https://bitbucket.org/mki5600/imap_python3

:Dependencies:

* Python3 [tested using ActivePython 3.3.2 (32-bit) on Win8 (64-bit)]
* imapclient ``pip install imapclient``

.. Note:: 

   See `<INSTALL.TXT>`_ for details on how to get the script working on Linux, 
   Mac OSX or Windows.

:Usage:

::

      $ imap_tools_light.py [-h] [-a] [-d] [-f] [-e] [-n] [--version] [-q | -v]
                            TOOL ACCOUNT [ACCOUNT ...]

Usage Examples
--------------

         (1) Test server response times of multiple IMAP servers:

             ``$ python3 imap_tools_light.py TEST ICLOUD GMAIL OUTLOOK``

         (2) Copy all messages from your Gmail account to Outlook.com, without
             copying any duplicate messages:

             ``$ python3 imap_tools_light.py -a -d MIGRATE GMAIL OUTLOOK``

         (3) Remove all duplicate messages from your ICLOUD INBOX (verbose):

             ``$ python3 imap_tools_light.py -v CLEAN ICLOUD``

           **OR**  Update **CONFIG_SECTION** with your own server and

         (4) Migrate all emails from any of "the big three" to an NSA-free zone:

             ``$ python3 imap_tools_light.py -a MIGRATE OUTLOOK YOURSERVER``


.. Note:: 

       Depending on your OS and Python installation you might have to use
       ``python3.X`` instead of ``python3``.


.. Hint::

       Works out of the box for ``GMAIL``, ``OUTLOOK``        
       and ``ICLOUD``, using default options. 

       For other servers and custom options, don't   
       forget to update the **CONFIG SECTION** at the    
       beginning of this script before you run it.   

Tools:
------

**LIST**

	    display list of mailboxes per account
    

**TEST**

	    list server response times for the most basic
	    IMAP server interactions (CONNECT, LOGIN, LIST FOLDERS,
	    SELECT FOLDERS)
    

**MAP**
    
	    returns automatic mailbox mapping of source and destination
	    account, computed using [-a (--all_mailboxes)] - output can be
	    pasted into config section and adapted before running MIGRATE
    

**MIGRATE**

	    copy emails from source mailbox(es) to
	    destination mailbox(es) Default: INBOX --> INBOX
    

**CLEAN**

	    delete or expunge (option [-e]) duplicate emails
	    in specified mailboxes (comparison based on message-id
	    or hashes of email header information) NO WARRANTY!
	    Default: INBOX
    

**DEL_EMPTIES**

	    delete empty mailboxes if they are not part of the
	    standard IMAP setup (i.e. INBOX, SENT, DRAFTS, TRASH)
    

.. Warning::

    Test TOOLS using ``[-n] --dry_run`` option,
    before applying any changes to your
    productive IMAP account.

positional arguments:
  TOOL                  choose between LIST, TEST, MAP, MIGRATE, CLEAN or
                        DEL_EMPTIES (descriptions above)
  ACCOUNT               List of unique IMAP account ids specified in config
                        section or optional imap_accounts.py. Tools MAP and
                        MIGRATE need exactly two accounts *source* ->
                        *destination*, the other tools will process one or
                        more accounts in sequence.

optional arguments:
  -h, --help            show this help message and exit
  -a, --all             process all mailboxes except "Trash" or empty folders
                        (DEFAULT: process only mailboxes specified in config
                        section)
  -d, --duplicate_filter
                        turn on duplicate email filter in MIGRATE mode
                        (DEFAULT: OFF)
  -f, --prepare_forum_post
                        turn on anonymous logging - user data will be
                        anonymised in console output with logging level .INFO
                        (DEFAULT: OFF) Supported tools: LIST and TEST
  -e, --expunge_deleted
                        permanently delete all emails with the flag "\deleted"
                        from your IMAP account WARNING: THIS ACTION CANNOT BE
                        UNDONE! (DEFAULT: OFF)
  -n, --dry-run         perform a trial run without copying or deleting any
                        messages(DEFAULT: OFF)
  --version             show program's version number and exit
  -q, --quiet           quiet (no logging output on console)
  -v, --verbose         more output (debug level on console)


::
  
   Thanks and Sources:
		      
   ********************************************************************************
   Special thanks to the sales team at wingware for supporting this project by
   issuing an 'Unpaid Open Source Development License' for Wing IDE 5.X:
   See: http://www.wingware.com for details.
   ********************************************************************************
   © 2013, Christoph Heer (BSD License)
	      
   imap_copy() and some ideas for dealing with IMAP connections
   (e.g. naming of constants and functions, use of setattr and dictionaries)
   were inspired by Christoph Heer's IMAPcopy script
   Source: https://github.com/jarus/imap_copy
   ********************************************************************************
   © 2010-2013 by Quentin Stafford-Fraser (GNU GPLv2 Lisence)
	      
   run_de_duplicate() and functions called subsequently, were adapted from
   a script called imapdedup.py by Quentin Stafford-Fraser
   Source: https://github.com/quentinsf/IMAPdedup
   ********************************************************************************
   © 2011 Jeff Preshing (no license specified)
     
   Timer (class) in combination with 'with' statements, adapted from a blogpost
   by Jeff Prshing
   Src: http://preshing.com/20110924/timing-your-code-using-pythons-with-statement/
   ********************************************************************************



#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
# imap_accounts.py
# :author: Markus Killer
# :copyright: September 2013
# :license: GNU GPLv3, see LICENSE for more details.
""".. |modtitle| replace:: IMAP Accounts
Dictionary of IMAP Accounts
"""
try:
    #: load private accounts not included in repository
    import imap_private_accounts
    ACCOUNTS_IN_FILE = imap_private_accounts.get_accounts()
except ImportError:
    #: container for IMAP server login details
    ACCOUNTS_IN_FILE = {}


#: Include as many accounts as practical for your purposes
#: remember that the account identifier
#: string needs to be unique (key of nested dictionary)
#: otherwise the account will be overwritten with
#: the latest credentials for that key.
ACCOUNTS_IN_FILE.update(
    {"TEST1":                              # unique account identifier type(str)
                                           # e.g. "GMAIL_XYZ" type(str)
    {'host': 'localhost',                  # e.g. imap.gmail.com type(str)
     'port': 143,                          # e.g. 993 type(int)
     'username': 'test1',                  # e.g. xyz@gmail.com type(str)
     'password': 'pass1',                  # e.g. password_for_gmail type(str)
                                           # EMPTY STRING: PASSWORD PROMPT
     'ssl': False                          # True type(bool)
    }})

ACCOUNTS_IN_FILE.update(
    {"TEST2":

    {'host': 'localhost',
     'port': 143,
     'username': 'test2',
     'password': 'pass2',

     'ssl':False
    }})

ACCOUNTS_IN_FILE.update(
    {"TEST3":

    {'host': 'localhost',
     'port': 143,
     'username': 'test3',
     'password': 'pass3',

     'ssl': False
    }})


def get_accounts():
    return ACCOUNTS_IN_FILE
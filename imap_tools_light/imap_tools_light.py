#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# imap_tools_light.py
# :author: Markus Killer
# :copyright: September 2013
# :license: GNU GPLv3, see LICENSES for more details.
""".. |modtitle| replace:: IMAP Tools Light for Python3

:Description: Migrate IMAP email messages from source mailbox(es) to destination 
mailbox(es) with an optional duplicate email filter. There are also some basic 
IMAP diagnostic tools and an experimental one that removes all duplicate emails 
in specified IMAP mailbox(es) or across all mailboxes of an account.

:Check for latest version on: https://bitbucket.org/mki5600/imap_python3

:Dependencies:

* Python3 [tested using ActivePython 3.3.2 (32-bit) on Win8 (64-bit)]
* imapclient ``pip install imapclient``

.. SEEALSO:: 

   `<INSTALL.TXT>`_ for details on how to get the script working on Linux, 
   Mac OSX or Windows.

:Usage:

::

      $ imap_tools_light.py [-h] [-a] [-d] [-f] [-e] [-n] [--version] [-q | -v]
                            TOOL ACCOUNT [ACCOUNT ...]

Usage Examples
--------------

         (1) Test server response times of multiple IMAP servers:

             ``$ python3 imap_tools_light.py TEST ICLOUD GMAIL OUTLOOK``

         (2) Copy all messages from your Gmail account to Outlook.com, without
             copying any duplicate messages:

             ``$ python3 imap_tools_light.py -a -d MIGRATE GMAIL OUTLOOK``

         (3) Remove all duplicate messages from your ICLOUD INBOX (verbose):

             ``$ python3 imap_tools_light.py -v CLEAN ICLOUD``

           **OR**  Update **CONFIG_SECTION** with your own server and

         (4) Migrate all emails from any of "the big three" to an NSA-free zone:

             ``$ python3 imap_tools_light.py -a MIGRATE OUTLOOK YOURSERVER``

.. NOTE:: 
       Depending on your OS and Python installation you might have to use
       ``python3.X`` instead of ``python3``.

"""
import argparse
import email
import hashlib
import imapclient
import os
import re
import sys
import textwrap
import time

import logging
from logging.handlers import RotatingFileHandler

from collections import defaultdict

try:
    import imap_accounts
    IMAP_ACCOUNTS = imap_accounts.get_accounts()
except ImportError:
    #: container for IMAP server login details
    IMAP_ACCOUNTS = {}


#: container for configuration runtime commandline arguments and options
#: as well as options specified in config section of this script
#: allows flexible implementation of new commanline options
#: or use of a text config file (if deployed as compiled executable)
_CONF = {}
#: container for active server connections
_CONN = {}
########################IMAP TOOLS LIGHT - CONFIGURATION########################

# CONFIGURATION OPTIONS FOR ALL TOOLS
#************************************

#: Login details for imap accounts (for tools MAP and MIGRATE
#: at least 2 accounts). Additional accounts can be stored in
#: optional file imap_accounts.py in the same folder.
#: Format: nested dictionary: {"unique_id":{login_details}}
#: If username/password is empty string, you will be prompted at runtime.
IMAP_ACCOUNTS.update(
    {"GMAIL":                              # unique account identifier type(str)
     # e.g. "GMAIL_XYZ" type(str)
     {'host': 'imap.gmail.com',             # e.g. imap.gmail.com type(str)
      'port': 993,                          # e.g. 993 type(int)
      'username': '',                       # e.g. xyz@gmail.com type(str)
      'password': '',                       # e.g. password_for_gmail / type(str)
      'ssl': True                           # True type(bool)
      }})

IMAP_ACCOUNTS.update(
    {"OUTLOOK":

     {'host': 'imap-mail.outlook.com',
      'port': 993,
      'username': '',
      'password': '',
      'ssl':True
      }})

IMAP_ACCOUNTS.update(
    {"ICLOUD":

     {'host': 'imap.mail.me.com',
      'port': 993,
      'username': '',
      'password': '',
      'ssl':True
      }})

#: For posts in public blogs or support forums it can be useful to anonymise 
#: user data in console output. The setting only affects print statements and 
#: console logging level .INFO). Will be overriden if command line option
#: ``[-f] --prepare_forum_post`` is used. (DEFAULT: False)
ANONYMOUS_LOGGING = False

#: If ``ANONYMOUS_LOGGING = True``, the following mailbox names will not be
#: anonymised as they do not reveil any private data (use upper case letters).
ANONYMOUS_MAILBOX_NAMES = ("INBOX", "DRAFTS", "TRASH", "SENT", "SENT MESSAGES",
                           "ARCHIVE")

# CONFIGURATION OPTIONS FOR CLEAN
#********************************

#: List of mailboxes to be scanned for duplicate
#: email messages
TO_BE_CLEANED_LIST = ['INBOX']

#: The following ways can be used to find duplicates:
#: level 1 - 'header-msg-id': message-ids (from parsed email header) -   (fast)
#: level 2 - 'header-md5': md5-hash of email header will be compared - (medium)
#: level 3 - 'header-sha1': sha1-hash of email header will be compared - (slow)
#:
#: hash collisions are highly unlikely but possible, i.e. theoretically, two
#: emails could receive the same hash-id. For details, see:
#: http://en.wikipedia.org/wiki/Comparison_of_cryptographic_hash_functions
COMPARISON_LEVEL = 'header-sha1'

#: Set "\\deleted" flag in batches of value specified
DEL_CHUNK_SIZE = 30

# CONFIGURATION OPTIONS FOR MIGRATE
#**********************************

#: Pair(s) of [('SOURCE_MAILBOX1','DESTINATION_MAILBOX1'), ...]
#: Emails will be copied from SOURCE_MAILBOX1 -> DESTINATION_MAILBOX1, ...
#: If you do not know exactly what to map run tool MAP first
#: and paste output here.
#: Example: imap_tools_light.py MAP ACCOUNT1 ACCOUNT2
#: Output: [('INBOX', 'INBOX'), ('SENT', 'SENT'), ('DRAFTS','DRAFTS')]
MAILBOX_MAPPING = [('INBOX', 'INBOX')]


#: Sometimes it might be desirable to create just one single mailbox archive
#: on the destination server (many web based Mail migration services work
#: this way - e.g. trueswitch.com used to create one folder 'GMAIL_Mail').
#: If set to `True` all source mailboxes specified will be mapped to the folder
#: `SOURCE-ACCOUNT-ID_Archive` on the destination server.
#: Warning:: all destination mailboxes in MAILBOX_MAPPING above will be
#: overwritten if `True`!
SINGLE_MAILARCHIVE = False

#: If set to `True` folder paths will be concatenated with `"_"`,
#: resulting in an all top-level folder structure on the destination
#: server (option included mainly for troubleshooting or fallback).
#: Warning:: all destination mailboxes in MAILBOX_MAPPING above will be
#: overwritten if `True`!
NO_SUBFOLDERS = False

#: Create DESTINATION_MAILBOX on destination server
#: if it does not exist (DEFAULT: `create_mailboxes = True`)
#: In combination with the commandline option [-a] (--all)
#: all mailboxes present on the source server will be created
#: on the destination server if set to `True`.
CREATE_MAILBOXES = True

#: Do not include these folders if commandline option [-a] (--all) is active.
#: By default the Trash folders are not included. You might have to add
#: the localised version of 'Trash' for your language.
EXCLUDE_MAILBOX_LIST = ['Trash', 'Papierkorb']

# LOGGING CONFIGURATION (OPTIONAL) - initialisation in 'if __name__ == "main"'
# See python documentation logging for details
# http://docs.python.org/3/library/logging
LOGFILE = 'tools.log'
LOGFILE_FMT = '%(asctime)s (%(msecs)3d) %(levelname)-8s' \
    '%(message)s [%(funcName)s: %(lineno)d]'
LOGFILE_DATEFMT = '%d/%m/%Y %H:%M:%S'
LOGFILE_MAX_BYTES = 4194304
LOGFILE_BACKUP_COUNT = 6

CONSOLE_FMT = '%(asctime)s %(name)-6s - %(message)s'
CONSOLE_DATEFMT = '%d/%m/%y %H:%M:%S'

################GENERALLY NO CHANGES NECESSARY BEYOND THIS POINT################
_exclude_list = []
for i in EXCLUDE_MAILBOX_LIST:
    _exclude_list.append(i.upper())
EXCLUDE_MAILBOX_LIST = set(_exclude_list)

ANON_SRV_RESPONSE = {
# host               process  re pattern   re replace  
"imap.mail.me.com": {"login":(r"User .+ logged", "User XY logged")},
"imap-mail.outlook.com": {"login":(r".+ authenticated", "XY authenticated")},
"imap.gmail.com": {"login":(r".+ authenticated", "XY authenticated")},
"imap.googlemail.com": {"login":(r".+ authenticated", "XY authenticated")},
}

##########################END OF CONFIGURATION SECTION##########################
#******************************************************************************#
#####################IMAP CONNECTIONS AND FOLDER OPERATIONS#####################

# logger_name: 'server'

def _connect(imap_account_id, target="IMAP"):

    imap_account = IMAP_ACCOUNTS[imap_account_id]

    imap_account = _get_credentials(imap_account, imap_account_id)

    _CONN.update({target: {'id': imap_account_id}})
    connect(imap_account, target)

    if '_conn' in _CONN[target] and _CONN[target]['state'] == 'AUTH':
        return True
    else:
        logger1.error("CONNECTION ERROR on %s server (%s): %s" % (target,
                                                                  imap_account['host'], _CONN[target]['state']))
        del _CONN[target]
        return False

def _connect_src_and_dest(src_id, dest_id):
    if _connect(src_id, "source") and _connect(dest_id, "destination"):
        return True
    else:
        return False

def _get_credentials(imap_account, imap_account_id):
    if imap_account['username'] == '':
        if _CONF['anonymous_logging']:
            print("\nWARNING: You will have to remove the following username "
                  "manually before posting console output to blog/forum\n")
        imap_account['username'] = input(
            'Enter username for {} --> '.format(imap_account_id))

    if imap_account['password'] == '':
        import getpass
        imap_account['password'] = getpass.getpass()
    return imap_account

def connect(imap_account, _conn_id):
    """connection states (imaplib.py for details):
    'NONAUTH': connected not logged in
    'AUTH': connected and logged in, in root folder
    'SELECTED': connected, logged in and in one of the server's mailboxes
    'LOGOUT': logged out, not connected
    """

    logger1.info("Connecting to %s server (%s)." % (_conn_id, imap_account['host']))

    try:
        with Timer() as t:
            connection = imapclient.IMAPClient(imap_account['host'],
                                               imap_account['port'],
                                               use_uid=True,
                                               ssl=imap_account['ssl']
                                               )
        _CONN[_conn_id].update(_conn=connection)
        _CONN[_conn_id].update(state=get_imap_state(connection))
    except Exception as e:
        _CONN[_conn_id].update(state=e)
        return False
    finally:
        log_interval(t.interval)
        if _CONF['tool'] == 'TEST':
            logger1.info("Connection state %s: ", _CONN[_conn_id]['state'])


    _CONN[_conn_id].update(login_attempts=1)
    if login(imap_account, _conn_id):
        return True



def login(imap_account, _conn_id):

    try:
        connection = _CONN[_conn_id]['_conn']
    except KeyError:
        logger1.error("%s server (%s): Login not possible, connection "
                      "not initialised."%  (_conn_id, connection.host))
        _CONN[_conn_id].update(state="LOGIN_FAILED")
        return False

    try:
        logger1.info("Authenticating at %s server (%s)." % (
            _conn_id, imap_account['host']))
        with Timer() as t:
            _login = connection.login(imap_account['username'],
                                      imap_account['password'])

        _CONN[_conn_id].update(state=get_imap_state(connection))
        if _CONF['tool'] == 'TEST':
            log_interval(t.interval)
            logger1.info("Connection state: %s" % _CONN[_conn_id]['state'])
            if _CONF['anonymous_logging']:
                try:
                    pattern = ANON_SRV_RESPONSE[imap_account['host']]['login'][0]
                    repl = ANON_SRV_RESPONSE[imap_account['host']]['login'][1]
                    _login = re.sub(pattern, repl, _login)
                except KeyError:
                    _login = "User XY " + _CONN[_conn_id]['state']
            logger1.info("Server response: %s" % _login)
        else:
            logger1.debug("Server response: %s" % _login)
        return True
    except Exception as e:
        logger1.error("%s server (%s): %s" %  (_conn_id, connection.host, e))

        if get_imap_state(connection) is "NONAUTH":
            if _CONN[_conn_id]['login_attempts'] == 3:
                logger1.error("%s server (%s): 3 unsuccessful password attempts)" %  (
                    _conn_id, imap_account['host']))
                _CONN[_conn_id].update(state="LOGIN_FAILED")
                del _CONN[_conn_id]['_conn']
                return False            
            _CONN[_conn_id]['login_attempts'] += 1
            if _CONF['anonymous_logging']:
                username = "***"
            else:
                username = imap_account['username']
            print(("Attempt {} of 3 for user {} on {}".format(
                _CONN[_conn_id]['login_attempts'],
                username, connection.host)))
            imap_account['password'] = ''
            imap_account = _get_credentials(imap_account, _conn_id)
            if login(imap_account, _conn_id):
                return True


def disconnect_all():
    _active_connections = [c for c in _CONN.keys()]
    for _conn_id in _active_connections:
        disconnect(_conn_id)

def disconnect(_conn_id):
    try:
        connection = _CONN[_conn_id]['_conn']
    except KeyError:
        del _CONN[_conn_id]
        return False

    _CONN[_conn_id].update(state=get_imap_state(connection))
    while _CONN[_conn_id]['state'] == 'SELECTED':
        close_mailbox(connection, _conn_id)
        _CONN[_conn_id].update(state=get_imap_state(connection))

    logout = connection.logout()
    logger1.info("Logging out of %s server (%s)." % (
        _conn_id, connection.host))
    logger1.debug("Server response: %s" % logout)
    # state update not necessary as connection will be deleted
    #_CONN[_conn_id].update('state', get_imap_state(connection))
    del _CONN[_conn_id]
    return True

def get_imap_state(connection):
    """connection states (imaplib.py for details):
    'NONAUTH': connected not logged in
    'AUTH': connected and logged in, in root folder
    'SELECTED': connected, logged in and in one of the server's mailboxes
    'LOGOUT': logged out, not connected
    """
    state = connection._imap.state
    return state

def select_mailbox(connection, mailbox, target, no_exit=False, log_open=True):
    """Select imap folder and return dictionary with imap server's response:

    ::
       mailbox_info = {'EXISTS': 3,
                      'PERMANENTFLAGS': ('\\Deleted', '\\Draft',
                                         '\\Seen', '\\Answered'),
                      'READ-WRITE': True, 'UIDNEXT': 4,
                      'FLAGS': ('\\Deleted', '\\Draft', '\\Seen', '\\Answered'),
                      'UIDVALIDITY': 1367354093,
                      'RECENT': 0}
    """
    if target not in ("source", "destination"):
        target = _CONN[target]['id']

    try:
        if log_open:
            logger1.info("Open %s mailbox [%s] on %s" % (
                target, mailbox, connection.host))

        with Timer() as t:
            mailbox_info =  connection.select_folder(mailbox)

    except Exception as e:
        if _CONF['dry_run']:
            logger1.info("Would open %s mailbox %s on %s: %s" %  (
                target, mailbox, connection.host, e))
            mailbox_info = {'EXISTS': 0,
                            'READ-WRITE': False,
                            'FLAGS': ('\\DRY_RUN')}

        else:
            logger1.error("Couldn't open %s mailbox %s on %s: %s" %  (
                target, mailbox, connection.host, e))
            mailbox_list = get_mailbox_list(connection, target, timer_on=False)
            mailbox_list = [m[2] for m in mailbox_list]
            if no_exit:
                logger1.debug("Output of list_folders: %s" % mailbox_list)
                mailbox_info = {'EXISTS': "unknown number of",
                                'READ-WRITE': False,
                                'FLAGS': ('\\SELECT_FAILED')}
            else:
                logger1.info("Check list of existing mailboxes: *%s*."
                             "Pick or create one and try again." %
                             "* *".join(mailbox_list))
                mailbox_info = {'EXISTS': "unknown number of",
                                'READ-WRITE': False,
                                'FLAGS': ('\\SELECT_FAILED')}
                sys.exit(1)
    finally:
        log_interval(t.interval)
        return mailbox_info

def delete_mailbox(connection, mailbox, target):
    if target not in ("source", "destination"):
        target = _CONN[target]['id']

    if _CONF['dry_run']:
        logger1.info("[-n] --dry-run: %s mailbox %s not deleted on %s." %
                     (target, mailbox, connection.host))
    else:
        try:
            logger1.info("Deleting mailbox %s on %s (%s)." %
                         (mailbox, target, connection.host))
            connection.delete_folder(mailbox)
        except Exception as e:
            logger1.error("Couldn't delete mailbox %s on %s (%s): %s" %  (
                mailbox, target, connection.host, e))




def _get_folder_delimiter(_conn_id):
    connection = _CONN[_conn_id]['_conn']
    try:
        delimiter = connection.namespace()[0][0][1]
    except:
        try:
            delimiter = get_mailbox_list(connection, _conn_id, False)[0][1]
        except:
            logger1.warning("Couldn't determine folder delimiter"
                            "on %s server (%s). Using fallback"
                            " delimiter: '_'."% (_conn_id, connection.host))
            delimiter = "_"

    return delimiter


def create_mailbox(connection, mailbox, target):
    if target not in ("source", "destination"):
        target = _CONN[target]['id']
    if _CONF['dry_run']:
        logger1.error("[-n] --dry-run: Would create %s mailbox %s on %s.)" %  (
            target, mailbox, connection.host))
    else:

        try:
            new_folder =  connection.create_folder(mailbox)
            logger1.info("Create %s mailbox %s on %s: %s" % (
                target, mailbox, connection.host, new_folder))
        except Exception as e:
            logger1.error("Couldn't create %s mailbox %s on %s: %s)" %  (
                target, mailbox, connection.host, e))
            sys.exit(1)


def close_mailbox(connection, target, log_close=True):
    if target not in ("source", "destination"):
        target = _CONN[target]['id']

    if get_imap_state(connection) == 'SELECTED':
        closed_folder =  connection.close_folder()
        if log_close:
            logger1.info("Close mailbox on %s server (%s): %s" % (
                target, connection.host, closed_folder))


def log_mailbox_info(mailbox_info, mailbox, target):
    if target not in ("source", "destination"):
        target = _CONN[target]['id']

    for k in sorted(mailbox_info.keys()):
        logger1.debug("Folder info (%s server - %s): %s: %s" % (target,
                                                                mailbox, k, mailbox_info[k]))

def check_mailbox_permissions(connection, mailbox, mailbox_info, target='IMAP'):
    if target not in ("source", "destination"):
        target = _CONN[target]['id']

    if mailbox_info['READ-WRITE'] is False:
        logger1.warning("Selected folder %s on %s server (%s) is not "
                        "writable!" % (mailbox, target, connection.host))

def get_mailbox_list(connection, _conn_id, xlist_folders=False, timer_on=True):
    mailboxlist = ""
    if xlist_folders:
        try:
            mailboxlist = connection.xlist_folders()
        except Exception as e:
            logger1.debug("Using fallback folder display - IMAP Server"
                          "doesn't support command: %s" % e)
    if mailboxlist == "":
        try:
            if timer_on:
                with Timer() as t:
                    mailboxlist = connection.list_folders()
            else:
                mailboxlist = connection.list_folders()
        except Exception as e:
            logger1.error("%s", e)
        finally:
            if timer_on:
                log_interval(t.interval)
    return mailboxlist

def _get_empty_mailbox_list(connection, imap_account_id, target="IMAP"):
    exclude_mailbox_list = _CONF['exclude_mailbox_list']
    empty_mailboxes = []


    mailbox_depth = {}
    mailboxlist = get_mailbox_list(connection, target, False)
    for m in mailboxlist:
        flags = m[0]
        delimiter = m[1]
        name = m[2]
        # do not process Trash folders or folders with flag 'Noselect'
        #if u'\\Noselect' in flags:
            #logger1.debug("%s not processed ('\\Noselect')" % name)
        if name.upper() in ('INBOX', 'POSTEINGANG'):
            logger1.debug("%s will not be deleted even if it's empty)" % name)
        elif name.upper() in exclude_mailbox_list:
            logger1.debug("%s not processed (in 'exclude_mailbox_list')" % name)
        else:
            depth = len(name.split(delimiter))
            try:
                mailbox_depth[depth].append(m)
            except KeyError:
                mailbox_depth[depth] = [m]
    parents = []
    if mailbox_depth != {}:

        max_depth = max([int(i) for i in mailbox_depth.keys()])
        print("Mailbox Stats on %s (%s) - Max depth = %d:" % (imap_account_id,
                                                              connection.host, depth))
        for k in sorted(iter(mailbox_depth.keys()), reverse=True):
            for m in mailbox_depth[k]:
                depth = k
                flags = m[0]
                delimiter = m[1]
                name = m[2]
                try:
                    no_msgs = connection.select_folder(name)['EXISTS']
                    deleted = connection.search("DELETED")
                    non_deleted = connection.search("NOT DELETED")
                    print("{0}\t -> Total Msgs: {1} Deleted: {2} Others: {3} Depth: {4}".format(name, no_msgs, len(deleted), len(non_deleted), depth))
                    try:
                        parents.append(name.split(delimiter)[-2])
                    except IndexError:
                        pass
                    if no_msgs == 0:
                        if len(deleted) > 0 and _CONF['expunge_deleted']:
                            connection.expunge()
                            logger3.info("[option EXPUNGE] The %d deleted messages "
                                         "have been permanently removed from %s "
                                         "before listing mailbox as empty"% (
                                             len(deleted), name))
                        empty_mailboxes.append(name)
                except Exception as e:
                    logger3.error("%s: %s" % (name, e))
                    if '\\Noselect' in flags and name not in parents:
                        empty_mailboxes.append(name)
                finally:
                    close_mailbox(connection, target, log_close=False)
    else:
        empty_mailboxes = []
        max_depth = 1

    return empty_mailboxes, max_depth






def  _get_clean_all_mailboxes(connection, _conn_id):
    exclude_mailbox_list = _CONF['exclude_mailbox_list']
    excluded = []

    mailboxlist = get_mailbox_list(connection, _conn_id, False)
    to_be_cleaned = []
    for m in mailboxlist:
        flags = m[0]
        delimiter = m[1]
        name = m[2]
        mailbox = name
        # do not map Trash folders or folders with flag 'Noselect'
        if '\\Noselect' in flags:
            logger1.debug("%s excluded" % name)
            excluded.append(name)
        elif '\\Trash' in flags:
            logger1.debug("%s excluded" % name)
            excluded.append(name)
        elif name.upper() in exclude_mailbox_list:
            logger1.debug("%s excluded" % name)
            excluded.append(name)
        else:
            try:
                if select_mailbox(connection,
                                  mailbox,
                                  "source", True)['EXISTS'] > 0:
                    to_be_cleaned.append(mailbox)
                else:
                    logger1.debug("Existing mailbox *%s* will not be cleaned: "
                                  "It is empty." % mailbox)

            except TypeError:
                logger1.debug("Existing mailbox *%s* could not be accessed. "
                              "Check name and encoding." % mailbox)
    return to_be_cleaned, set(excluded)


def _get_mailbox_mapping():

    if _CONF['all_mailboxes']:
        MAILBOX_MAPPING = get_src_mailbox_mapping()
    else:
        if _CONF['single_mailarchive'] or _CONF['no_subfolders']:
            MAILBOX_MAPPING = _options_to_custom_mailbox_mapping(
                _CONF['mailbox_mapping'])
        else:
            MAILBOX_MAPPING = _CONF['mailbox_mapping']

    return MAILBOX_MAPPING

def _options_to_custom_mailbox_mapping(_custom_mapping):

    src_connection = _CONN['source']['_conn']
    dest_connection = _CONN['destination']['_conn']

    delimiter = _get_folder_delimiter("source")

    _mailbox_mapping = []
    single_dest_mailbox = _CONN['source']['id'] + "_Archive"

    for _src_mailbox, __ in _custom_mapping:
        try:
            if select_mailbox(src_connection,
                              _src_mailbox,
                              "source", True)['EXISTS'] > 0:
                if _CONF['single_mailarchive']:
                    dest_mailbox = single_dest_mailbox
                else:
                    dest_mailbox = _src_mailbox
                    if _CONF['no_subfolders']:
                        try:
                            dest_mailbox = "_".join(
                                dest_mailbox.split(delimiter))
                        except IndexError:
                            dest_mailbox = dest_mailbox

                if "gmail" not in dest_connection.host or \
                   "google" not in dest_connection.host:
                    dest_mailbox = re.sub(r"\]|\[", r"", dest_mailbox)
                _mailbox_mapping.append((_src_mailbox, dest_mailbox))
                close_mailbox(src_connection, "source")
            else:
                logger1.debug("Existing mailbox *%s* will not be mapped: "
                              "It is empty." % _src_mailbox)

        except TypeError:
            logger1.debug("Existing mailbox *%s* could not be mapped. "
                          "Check name and encoding." % _src_mailbox)

    logger3.debug("Options applied to custom mapping: %s" % _mailbox_mapping)
    return _mailbox_mapping

def get_src_mailbox_mapping():

    src_connection = _CONN['source']['_conn']
    dest_connection = _CONN['destination']['_conn']

    _dest_delimiter = _get_folder_delimiter("destination")

    exclude_mailbox_list = _CONF['exclude_mailbox_list']

    src_mailboxlist = get_mailbox_list(src_connection, "source", False)

    complete_mailbox_mapping = []
    single_dest_mailbox = _CONN['source']['id'] + "_Archive"
    for m in src_mailboxlist:
        flags = m[0]
        delimiter = m[1]
        name = m[2]
        mailbox = name
        # do not map Trash folders or folders with flag 'Noselect'
        if '\\Noselect' in flags:
            logger1.debug("%s excluded from mailbox mapping" % name)
        elif '\\Trash' in flags:
            logger1.debug("%s excluded from mailbox mapping" % name)
        elif name.upper() in exclude_mailbox_list:
            logger1.debug("%s excluded from mailbox mapping" % name)
        else:
            try:
                if select_mailbox(src_connection,
                                  mailbox,  "source",
                                  no_exit=True, log_open=False)['EXISTS'] > 0:
                    if _CONF['single_mailarchive']:
                        dest_mailbox = single_dest_mailbox
                    else:
                        dest_mailbox = mailbox
                        if _CONF['no_subfolders']:
                            try:
                                dest_mailbox = "_".join(
                                    dest_mailbox.split(delimiter))
                            except IndexError:
                                dest_mailbox = dest_mailbox
                        else:
                            try:
                                dest_mailbox = _dest_delimiter.join(
                                    dest_mailbox.split(delimiter))

                                # re.sub raises bogus escape EOL error
                                #dest_mailbox = re.sub(delimiter,
                                #                      _dest_delimiter,
                                #                      dest_mailbox)

                            except IndexError:
                                dest_mailbox = dest_mailbox

                    if "gmail" not in dest_connection.host:
                        dest_mailbox = re.sub(r"\]|\[", r"", dest_mailbox)
                    complete_mailbox_mapping.append((mailbox, dest_mailbox))
                    close_mailbox(src_connection, "source", log_close=False)
                else:
                    logger1.debug("Existing mailbox *%s* will not be mapped: "
                                  "It is empty." % mailbox)

            except TypeError:
                logger1.debug("Existing mailbox *%s* could not be mapped. "
                              "Check name and encoding." % mailbox)

    logger1.debug("Source mailbox list: %s" % src_mailboxlist)
    logger1.debug("Suggested mapping: %s" % complete_mailbox_mapping)
    return complete_mailbox_mapping

####################################MESSAGES####################################

# logger_name: 'email'

def _get_human_readable(size,precision=2):
    suffixes = ['B','KB','MB','GB','TB']
    suffixIndex = 0
    while size > 1024:
        suffixIndex += 1 #increment the index of the suffix
        size = size/1024.0 #apply the division
    return "%.*f %s"%(precision, size, suffixes[suffixIndex])

def _truncate_str(long_string, length=12):
    if "Unknown" in long_string:
        short_string = long_string
    elif len(long_string) > length:
        short_string = long_string[:length] + " ..."
    else:
        short_string = long_string
    return short_string

def get_sorted_msg_ids(connection, mailbox, target, sort_criteria=['DATE'],
                       search_criteria=["NOT DELETED"]):
    """Returns a sorted list of message ids, according to search and
sort criteria specified.

Selection of sort criteria
(for details see: http://tools.ietf.org/html/rfc5256)::

    'ARRIVAL', 'CC', 'DATE', 'FROM', 'SIZE', 'SUBJECT', 'TO'
    'REVERSE ARRIVAL', 'REVERSE CC', ...

Selection of search criteria:
(for details see: http://tools.ietf.org/html/rfc3501#section-6.4.4)::

    'ALL', 'ANSWERED', 'DRAFT', 'DELETED', 'FLAGGED', 'RECENT',
    'SINCE 1-Feb-2005', ...
    or
    'NOT ANSWERED', 'NOT DELETED', 'NOT ...', ...

.. warning::

    Server needs to support IMAP4Rev1.
"""
    if target not in ("source", "destination"):
        target = _CONN[target]['id']

    if connection.has_capability('SORT'):
        logger2.info("Looking for emails in %s on %s server (%s). "
                     "Search criteria: '%s' Sort criteria: '%s'" % (
                         mailbox, target, connection.host,
                         "', '".join(search_criteria), "', '".join(sort_criteria)))
        try:
            msg_ids = connection.sort(sort_criteria, search_criteria)
        except Exception as e:
            logger2.error(
                "Messages in %s mailbox %s on %s couldn't be retrieved: %s)" %(
                    target, mailbox, connection.host, e))
            sys.exit(1)
        return msg_ids
    else:
        logger2.debug("SORT unavailable on server, using fallback")
        fallback = get_msg_ids(connection, mailbox, target, search_criteria)
        return fallback

#: level 1 - 'header-msg-id': message-ids (from parsed email header) -   (fast)
#: level 2 - 'header-md5': md5-hash of email header will be compared - (medium)
#: level 3 - 'header-sha1': sha1-hash of email header will be compared - (slow)

def get_msg_identifier(msg_header):
    msg_header = msg_header.encode('utf-8')
    if _CONF['comparison_level'] == "header-msg-id":
        msg_identifier =  parse_msg_header(msg_header, 'Message-Id')
        if msg_identifier in ("UnknownParseErr", "UnknownErr", ""):
            logger3.warning("Message '%s' dated '%s' has no "
                            "Message-ID header." %(
                                parse_msg_header(msg_header, 'Subject'),
                                parse_msg_header(msg_header, 'Received')
                            ))
            logger3.warning("You might want to use a different comparison "
                            "level ('header-md5' or 'header-sha1')")
            return None

    elif _CONF['comparison_level'] == "header-md5":
        md5 = hashlib.md5()
        md5.update(msg_header)
        msg_identifier = md5.hexdigest()
    elif _CONF['comparison_level'] == "header-sha1":
        sha1 = hashlib.sha1()
        sha1.update(msg_header)
        msg_identifier = sha1.hexdigest()

    return msg_identifier

def get_msg_ids(connection, mailbox, target, criteria=["NOT DELETED"]):
    """Returns a sorted list of message ids, according to search
criteria specified.

Selection of search criteria:
(for details see: http://tools.ietf.org/html/rfc3501#section-6.4.4)::

    'ALL', 'ANSWERED', 'DRAFT', 'DELETED', 'FLAGGED', 'RECENT',
    'SINCE 1-Feb-2005', ...
    or
    'NOT ANSWERED', 'NOT DELETED', 'NOT ...', ...
"""

    logger2.info("Looking for emails in %s on %s server (%s). "
                 "Search criteria: '%s'" % (
                     mailbox, target, connection.host, "', '".join(criteria)))
    try:
        msg_ids = connection.search(criteria)
    except Exception as e:
        logger2.error(
            "Messages in %s mailbox %s on %s couldn't be retrieved: %s)" %(
                target, mailbox, connection.host, e))
        sys.exit(1)
    return msg_ids

def get_message_info(response):

    # data retrieved by imapclient / imaplib
    msg_header = response['RFC822.HEADER']
    msg_flags = ";".join([f[1:] for f in response['FLAGS']])
    msg_time = response['INTERNALDATE']
    msg_size_bytes = response['RFC822.SIZE']
    msg_size_human = _get_human_readable(msg_size_bytes, precision=2)


    # use python's built-in email parser to parse message header
    # options: 'Subject', 'Delivered-To', 'From', 'To', 'Received',
    #          'Received-SPF', 'MIME-Version', 'Message-Id', 'Return-Path',
    #          'X-Email-Type-Id', 'X-Received'
    msg_from = parse_msg_header(msg_header, "From")
    msg_to = parse_msg_header(msg_header, "To")
    msg_delivered_to =  parse_msg_header(msg_header, "Delivered-To")
    msg_subject = email.header.decode_header(parse_msg_header(msg_header, "Subject"))
    if msg_subject[0][1] is not None:
        try:
            msg_subject = msg_subject[0][0].decode(msg_subject[0][1])
        except UnicodeDecodeError:
            msg_subject = "UnknownDecodeErr"
    else:
        msg_subject = msg_subject[0][0] 
    msg_id_in_header =  parse_msg_header(msg_header, "Message-Id")

    msg_info = {'flags': msg_flags,
                'time': msg_time,
                'size_bytes': msg_size_bytes,
                'size_human': msg_size_human,
                'from': msg_from,
                'to': msg_to,
                'delivered_to' : msg_delivered_to,
                'subject': msg_subject,
                'header_id': msg_id_in_header
                }

    return msg_info

def log_message_info(msg_info):
    logger2.debug(
        "Message info <Date: %s, From: %s, To: %s, "
        "Subject: %s ID: %s>" % (msg_info['time'],
                                 _truncate_str(msg_info['from']),
                                 _truncate_str(msg_info['to']),
                                 _truncate_str(msg_info['subject'], 15),
                                 _truncate_str(msg_info['header_id'])))

def parse_msg_header(msg_header, msg_header_key):
    """Returns dictionary value of parsed message header.

    Common msg_header_keys:

    'Subject', 'Delivered-To', 'From', 'To', 'Received', 'Received-SPF',
    'MIME-Version', 'Message-Id', 'Return-Path', 'X-Email-Type-Id', 'X-Received'
    """
    m = email.message_from_string(msg_header)

    #for k in sorted(m.keys()):
        #v = re.sub(r"\s", " ", m[k]).strip()
        #print "\n%s: %s" % (k, v)

    try:
        v = m[msg_header_key]
        # make sure that there are no linebreaks within the header fields
        v = re.sub(r"\s", " ", str(v))
    except KeyError:
        v = "UnknownParseErr"
    except:
        v = "UnknownErr"
    return v.strip()

def get_message(connection, msg_id, specs, target):
    """returns server response as dictionary. Items listed in specs will
    be returned: e.g.
    `specs = ['INTERNALDATE', 'FLAGS', 'RFC822.HEADER','RFC822.SIZE', 'RFC822']`
    `response -> {'INTERNALDATE':'2013-04-28 22:01:28', ...}`"""
    if target not in ("source", "destination"):
        target = _CONN[target]['id']
    try:
        response = connection.fetch(int(msg_id), specs)[msg_id]
    except Exception as e:
        logger2.error("Message (ID: %d) on %s server (%s) could "
                      "not be fetched: %s" % (int(msg_id), target,
                                              connection.host, e))
        return None, False

    return response, True

def copy_message(connection, destination_mailbox, msg,
                 flags, msg_time, target='destination'):
    if target not in ("source", "destination"):
        target = _CONN[target]['id']

    if _CONF['dry_run']:
        logger2.info("[-n] --dry-run: Message not copied to %s." %
                     destination_mailbox)
    else:
        try:
            connection.append(destination_mailbox, msg, flags, msg_time)
        except Exception as e:
            logger2.error("Message could not be copied to %s "
                          "server (%s): %s" % (target, connection.host, e))


def check_number_of_msg_ids(msg_id_count, folder_info_exists):
    if msg_id_count == folder_info_exists:
        logger2.debug("Number of retrieved message ids matches "
                      "server's folder info.")
    else:
        logger2.warning("Number of retrieved message ids does NOT match "
                        "server's folder info. Possible causes: User choice "
                        "in search criteria (e.g. 'SINCE 1-Feb-2011'), "
                        "new arrivals or deleted messages in mailbox." )
        logger2.warning("")
        logger2.debug("Folder information: %d Retrieved ids: %d" % (
            folder_info_exists, msg_id_count))


###################################IMAP TOOLS###################################

# logger_name: 'tools'

def run_list_mailboxes(imap_account_id, _conn_id='IMAP'):
    """
    display list of mailboxes per account
    """

    connection = _CONN[_conn_id]['_conn']

    mailbox_list_raw = get_mailbox_list(connection, imap_account_id)
    # folder flags: m[0], delimiter: m[1], name: m[2]
    mailbox_list = [m[2] for m in mailbox_list_raw if '\\Noselect' not in m[0]]
    print("\nAccount: ", imap_account_id, end=' ')
    username = IMAP_ACCOUNTS[imap_account_id]["username"]
    if _CONF["anonymous_logging"]:
        try:
            username = username.split('@')
            username = "***@" + username[1]
        except IndexError:
            username = "***"
    else:
        username = username
    print("\t\t\tUser:\t", username)
    print("Server:\t", connection.host)
    print("\nMailboxes:")
    custom_mailbox_counter = 0
    for mailbox in mailbox_list:
        mailbox_info = select_mailbox(connection, mailbox, "IMAP",
                                      no_exit=True, log_open=False)
        if mailbox.upper() not in ANONYMOUS_MAILBOX_NAMES \
           and _CONF["anonymous_logging"]:
            custom_mailbox_counter += 1
            mailbox = "Custom" + str(custom_mailbox_counter)
        else:
            mailbox = mailbox
        print("\t{} ({} messages)".format(mailbox, mailbox_info['EXISTS']))
        close_mailbox(connection, "IMAP", log_close=False)
    print("\n")
    logger3.debug("Mailboxes on %s server (%s): %s" %
                  (imap_account_id, connection.host, mailbox_list_raw))
    disconnect(_conn_id)
    return True

def run_test_response(imap_account_id):
    """
    list server response times for the most basic
    IMAP server interactions (CONNECT, LOGIN, LIST FOLDERS,
    SELECT FOLDERS)
    """

    logger3.info("TESTING %s ...", imap_account_id)

    if _connect(imap_account_id):

        connection = _CONN["IMAP"]['_conn']

        logger3.info("Retrieving folder list ...")
        mailbox_list_raw = get_mailbox_list(connection, imap_account_id)
        # folder flags: m[0], delimiter: m[1], name: m[2]
        mailbox_list = [m[2] for m in mailbox_list_raw if '\\Noselect' not in m[0]]

        logger3.info("There are {} folders for this user on {}.".format(
            len(mailbox_list), connection.host))
        logger3.info("Timing SELECT command for all folders ...")
        mailbox_counter = 0
        custom_mailbox_counter = 0
        for mailbox in mailbox_list:
            if mailbox.upper() not in ANONYMOUS_MAILBOX_NAMES \
               and _CONF["anonymous_logging"]:
                custom_mailbox_counter += 1
                mailbox_log_name = "Custom" + str(custom_mailbox_counter)
            else:
                mailbox_log_name = mailbox          
            mailbox_counter += 1
            logger3.info("SELECT [%s] (%d of %d)" % (mailbox_log_name, mailbox_counter,
                                                     len(mailbox_list)))
            mailbox_info = select_mailbox(connection, mailbox, "IMAP",
                                          no_exit=True, log_open=False)
            if type(mailbox_info['EXISTS']) is int:
                logger3.info("There are %d messages in [%s]" %(
                    mailbox_info['EXISTS'], mailbox_log_name))
                close_mailbox(connection, "IMAP", log_close=False)
            else:
                logger3.info("There is an %s messages in [%s]" %(
                    mailbox_info['EXISTS'], mailbox_log_name))




def run_delete_empty_mailboxes(imap_account_id, _conn_id="IMAP"):
    """
    delete empty mailboxes if they are not part of the
    standard IMAP setup (i.e. INBOX, SENT, DRAFTS, TRASH)
    """

    connection = _CONN[_conn_id]['_conn']
    empty_mailboxes, depth = _get_empty_mailbox_list(connection, imap_account_id)
    if empty_mailboxes != []:
        for m in empty_mailboxes:
            logger3.info("Delete empty mailbox %s on %s (%s)." %
                         (m, _CONN[_conn_id]['id'], connection.host))
            delete_mailbox(connection, m, _conn_id)
        for i in range(depth-1):
            logger3.debug("Re-scanning mailboxes for empty Parent folders ...")
            empty_mailboxes, __ = _get_empty_mailbox_list(connection, imap_account_id)
            if empty_mailboxes != []:
                for m in empty_mailboxes:
                    logger3.info("Delete empty mailbox %s on %s (%s)." %
                                 (m, _CONN[_conn_id]['id'], connection.host))
                    delete_mailbox(connection, m, _conn_id)
            else:
                print("There are no empty Parent mailboxes on %s (%s)" % (imap_account_id,
                                                                          connection.host))
    else:
        print("There are no deletable empty mailboxes on %s (%s)" % (imap_account_id,
                                                                     connection.host))



def run_map_mailboxes():
    """    
    returns automatic mailbox mapping of source and destination
    account, computed using [-a (--all_mailboxes)] - output can be
    pasted into config section and adapted before running MIGRATE
    """

    _mailbox_mapping = get_src_mailbox_mapping()

    print("\nMailbox Mapping Suggestion {0} ({1}) --> {2} ({3}):\n" \
          "(can be copy-pasted into the config section)\n\n".format(
              _CONN['source']['id'], _CONN['source']['_conn'].host,
              _CONN['destination']['id'], _CONN['destination']['_conn'].host))
    print("MAILBOX_MAPPING = {0}\n\n".format(_mailbox_mapping))


def run_migration():
    """
    copy emails from source mailbox(es) to
    destination mailbox(es) Default: INBOX --> INBOX
    """

    MAILBOX_MAPPING = _get_mailbox_mapping()

    for source_mailbox, destination_mailbox in MAILBOX_MAPPING:

        imap_copy(source_mailbox, destination_mailbox)

#: Adapted from imapdedup.py (published under GNU GPLv2)
#: (c) 2010-2013 by Quentin Stafford-Fraser
#: Source: https://github.com/quentinsf/IMAPdedup
def run_de_duplicate(imap_id, _conn_id="IMAP"):
    """
    delete or expunge (option [-e]) duplicate emails
    in specified mailboxes (comparison based on message-id
    or hashes of email header information) NO WARRANTY!
    Default: INBOX
    """

    msg_ids = {}  # should be be the same across all mailboxes of the same
                    # account (stores the list of previously seen message IDs)

    # make the dictionary of previously seen messages available accross
    # different functions, without having to pass it on as an argument
    setattr(de_duplicate, "previously_seen", msg_ids)

    comparison_level = _CONF['comparison_level']
    connection = _CONN[_conn_id]['_conn']
    if _CONF['all_mailboxes']:
        to_be_cleaned, excluded = _get_clean_all_mailboxes(connection, _conn_id)
    else:
        excluded = []
        to_be_cleaned = _CONF['to_be_cleaned_list']

    #: iterate through a set of named mailboxes and delete the later messages
    #: discovered
    for mailbox in to_be_cleaned:
        if mailbox not in _CONF['exclude_mailbox_list']:
            logger3.info("Cleaning: %s on %s server (%s) ... "
                         "Comparison level: %s"% (
                             mailbox, _conn_id, connection.host,
                             comparison_level))
            de_duplicate(connection, mailbox, _conn_id)
        else:
            excluded.append(mailbox)

    # Delete dictionary of previously seen message IDs before moving on to
    # next IMAP account.
    delattr(de_duplicate, "previously_seen")

    if len(excluded) > 0:
        #logger3.debug("The following mailboxes were excluded specified in 'exclude_mailbox_list' or with flag '\Noselect'")
        logger3.debug(r"The following mailboxes were excluded by server "
                      r"(with flag '\Noselect') or as specified "
                      r"in 'exclude_mailbox_list': %s", excluded)

    logger3.info("Finished cleaning: %s on %s server (%s) ..." % (
        to_be_cleaned, _conn_id, connection.host))


#: This function and some ideas for dealing with IMAP connections
#: (e.g. naming of constants and functions, use of setattr)
#: were inspired by Christoph Heer's IMAPcopy script published under
#: BSE License https://github.com/jarus/imap_copy
def imap_copy(source_mailbox, destination_mailbox):

    source_connection = _CONN['source']['_conn']
    destination_connection = _CONN['destination']['_conn']

    # Open source mailbox
    src_folder_info = select_mailbox(source_connection,
                                     source_mailbox, "source")
    if logger1.getEffectiveLevel() == 10:  # 10 -> 'DEBUG'-Level
        log_mailbox_info(src_folder_info, source_mailbox, "source")

    # Connect to destination and create mailbox (optional)
    if not destination_connection.folder_exists(destination_mailbox) \
       and _CONF['create_mailboxes']:
        create_mailbox(destination_connection,
                       destination_mailbox, "destination")
        dest_folder_info = select_mailbox(destination_connection,
                                          destination_mailbox, "destination")
    else:
        dest_folder_info = select_mailbox(destination_connection,
                                          destination_mailbox, "destination")

    if logger1.getEffectiveLevel() == 10:  # 10 -> 'DEBUG'-Level
        log_mailbox_info(dest_folder_info, destination_mailbox, "destination")

    # Warn if no write access
    check_mailbox_permissions(destination_connection, destination_mailbox,
                              dest_folder_info, "destination")

    if _CONF['duplicate_filter']:
        msg_ids = {}
        setattr(de_duplicate, "previously_seen", msg_ids)
        comparison_level = _CONF['comparison_level']
        logger3.info("Checking for duplicates in mailbox %s on %s server (%s) ... "
                     "Comparison level: %s"% (
                         destination_mailbox, "destination", destination_connection.host,
                         comparison_level))
        de_duplicate(destination_connection, destination_mailbox, "destination")


    # Look for mails in source mailbox
    # if server supports SORT command (IMAP4Rev1), ids will be sorted
    # According to the first criteria list (['DATE'])
    msg_ids = get_sorted_msg_ids(source_connection, source_mailbox, "source",
                                 #                                ['DATE'], ['SINCE 1-Jan-2005', 'BEFORE 1-Jan-2007'])
                                 ['DATE'], ['NOT DELETED'])
    mail_count = len(msg_ids)

    # Warn if folder info doesn't match number of retrieved message ids
    # Possible causes: new arrivals or deleted messages still in mailbox
    check_number_of_msg_ids(mail_count, src_folder_info['EXISTS'])

    logger1.info("Start migration %s => %s (%d mails)" % (
        source_mailbox, destination_mailbox, mail_count))

    # define server response: list items will be returned in response
    # dictionary see built-in imaplib.py for options
    # Important: 'RFC822' is the complete message, the rest is metadata
    if _CONF['duplicate_filter']:
        # get header data to check for duplicates
        specs = ['INTERNALDATE', 'FLAGS', 'RFC822.HEADER',
                 'RFC822.SIZE']
    else:
        # get complete email data if not checking for duplicates
        specs = ['INTERNALDATE', 'FLAGS', 'RFC822.HEADER',
                 'RFC822.SIZE', 'RFC822']

    for i, msg_id in enumerate(msg_ids):

        response, fetch_success = get_message(source_connection,
                                              msg_id, specs, "source")

        if response is not None:
            msg_info = get_message_info(response)
            if logger1.getEffectiveLevel() == 10:  # 10 -> 'DEBUG'-Level
                log_message_info(msg_info)
        else:
            msg_info['size_human'] = 'unknown'

        logger1.info("Copy mail %d of %d (Size: %s)" % (i+1, mail_count,
                                                        msg_info['size_human']))

        # Copy email to destination folder. If option [-d] --duplicate_filter
        # is selected, duplicate messages are not copied to destination
        if fetch_success:
            if _CONF['duplicate_filter']:
                if check_for_duplicate(source_mailbox, msg_id, response) is False:
                    specs = ['INTERNALDATE', 'FLAGS', 'RFC822.HEADER',
                             'RFC822.SIZE', 'RFC822']
                    response, fetch_full_success = get_message(source_connection,
                                                               msg_id, specs, "source")
                    if fetch_full_success:
                        msg = response['RFC822']
                        flags = response['FLAGS']
                        msg_time = response['INTERNALDATE']
                        copy_message(destination_connection, destination_mailbox,
                                     msg, flags, msg_time, "destination")

            else:
                msg = response['RFC822']
                flags = response['FLAGS']
                msg_time = response['INTERNALDATE']
                copy_message(destination_connection, destination_mailbox,
                             msg, flags, msg_time, "destination")

    logger1.info("Migration complete %s => %s (%d mails)" % (
        source_mailbox, destination_mailbox, mail_count))

    if _CONF['duplicate_filter']:
        delattr(de_duplicate, "previously_seen")

def de_duplicate(connection, mailbox, _conn_id):

    msg_ids = getattr(de_duplicate, "previously_seen")

    msgs_to_delete = []  # should be reset for each mailbox
    setattr(de_duplicate, "msgs_to_delete", msgs_to_delete)
    msg_map = {}  # should be reset for each mailbox
    setattr(de_duplicate, "msg_map", msg_map)

    # open mailbox and return number of messages in mailbox

    msgs = select_mailbox(connection, mailbox, _conn_id)['EXISTS']


    if msgs > 0:


        logger3.info("There are %d messages in %s." % (msgs, mailbox))

        deleted = get_sorted_msg_ids(connection, mailbox, _conn_id,
                                     sort_criteria=['DATE'],
                                     search_criteria=["DELETED"])
        numdeleted = len(deleted)
        logger3.info("%s meassge(s) currently marked as deleted in %s" %
                     (numdeleted, mailbox))

        msgnums = get_sorted_msg_ids(connection, mailbox, _conn_id,
                                     sort_criteria=['DATE'],
                                     search_criteria=["NOT DELETED"] )
        logger3.info("%d others in %s" % (len(msgnums), mailbox))
        logger3.info("Reading the others ... (this may take a while - use "
                     "option [-v] to see progress)")

        # define server response: list items will be returned in response
        # dictionary see built-in imaplib.py for options
        # Important: 'RFC822' is the complete message, the rest is metadata
        specs = ['INTERNALDATE', 'FLAGS', 'RFC822.HEADER',
                 'RFC822.SIZE']

        for mnum in msgnums:
            response, fetch_success = get_message(connection,
                                                  mnum, specs, _conn_id)
            if fetch_success:
                compare_msg(connection, _conn_id, mailbox, mnum, response)

        if len(msgs_to_delete) == 0:
            logger3.info("No duplicates were found in %s" % mailbox)
        else:
            logger3.info("There are %d duplicate messages in %s" % (
                len(msgs_to_delete), mailbox))

            #logger3.debug("Message info of all duplicate messages:")
            #for mnum in msgs_to_delete:
                #if logger3.getEffectiveLevel() == 10:  # 10 -> 'DEBUG'-Level
                    #log_message_info(msg_map[str(mnum)])


            if _CONF['dry_run']:

                logger3.info("If you had not selected the '[-n] --dry-run' "
                             "option,\nthese messages would be marked "
                             "as 'deleted'.")

            else:

                logger3.info("Marking messages as deleted ... ")
                chunk_size = _CONF['del_chunk_size']
                logging.debug("(in batches of %d)" % chunk_size)
                for i in range(0, len(msgs_to_delete), chunk_size):
                    message_ids = ','.join(msgs_to_delete[i:i + chunk_size])
                    connection.add_flags(message_ids, "\Deleted")
                    logger3.debug("Batch starting at item %d marked." % i)

                logger3.info("Confirming new numbers ...")
                deleted = get_sorted_msg_ids(connection, mailbox, _conn_id,
                                             sort_criteria=['DATE'],
                                             search_criteria=["DELETED"])
                numdeleted = len(deleted)
                undeleted = get_sorted_msg_ids(connection, mailbox, _conn_id,
                                               sort_criteria=['DATE'],
                                               search_criteria=["NOT DELETED"])
                numundel = len(undeleted)
                logger3.info("There are now %d meassge(s) currently "
                             "marked as deleted and %d others in %s" % (
                                 numdeleted, numundel, mailbox))
                if _CONF['expunge_deleted']:
                    connection.expunge()
                    logger3.info("[option EXPUNGE] The %d duplicate messages "
                                 "have been permanently removed from %s" % (
                                     numdeleted, mailbox))

    else:
        logger3.info("There are no messages in %s. Duplicate check skipped."% (mailbox))
    delattr(de_duplicate, "msgs_to_delete")
    delattr(de_duplicate, "msg_map")


def check_for_duplicate(mailbox, mnum, response):
    msg_ids = getattr(de_duplicate, "previously_seen")
    msg_info = get_message_info(response)
    msg_header = response['RFC822.HEADER']
    logger3.debug("Checking message %s %s" %(mailbox, mnum))
    msg_id = get_msg_identifier(msg_header)
    if msg_id is not None:
        if msg_id in msg_ids:
            logger3.info("Message %s_%s is a duplicate of %s and %s "
                         "not be copied." % (
                             mailbox, mnum, msg_ids[msg_id],
                             _CONF['dry_run'] and "would" or "will") )
            return True

        else:
            msg_ids[msg_id] = mailbox + '_' + str(mnum)
            return False


def compare_msg(connection, _conn_id, mailbox, mnum, response):

    msg_ids = getattr(de_duplicate, "previously_seen")
    msgs_to_delete = getattr(de_duplicate, "msgs_to_delete")
    msg_map = getattr(de_duplicate, "msg_map")

    msg_info = get_message_info(response)

    msg_header = response['RFC822.HEADER']
    logger3.debug("Checking message %s %s" %(mailbox, mnum))
    msg_id = get_msg_identifier(msg_header)
    msg_map[str(mnum)] = msg_info
    if msg_id is not None:
        if msg_id in msg_ids:
            logger3.info("Message %s_%s is a duplicate of %s and %s "
                         "be marked as deleted." % (
                             mailbox, mnum, msg_ids[msg_id],
                             _CONF['dry_run'] and "would" or "will") )
            if logger3.getEffectiveLevel() == 10:  # 10 -> 'DEBUG'-Level
                log_message_info(msg_info)

            msgs_to_delete.append(str(mnum))
        else:
            msg_ids[msg_id] = mailbox + '_' + str(mnum)

def start_and_config_tools(tool, account_ids, config={}, accounts={}):

    # loads defaults for calls from python module
    if "called_from" not in _CONF:
        # load additional imap accounts if supplied
        if accounts != {}:
            IMAP_ACCOUNTS.update(accounts)

        # set defaults for calls from other python modules
        # can be overridden by supplying an extra config dict
        # only changed values need to be in that dict
        _CONF.update(
            {
                "called_from": "python_module",
                "tool": tool.upper(),
                "all_mailboxes": False,
                "duplicate_filter": False,
                "expunge_deleted": False,
                "dry_run": False,
                "anonymous_logging": ANONYMOUS_LOGGING
            }
        )    

    # load options specified in config section
    # (for commandline and calls from other python modules)
    _CONF.update(
        {
            "to_be_cleaned_list": TO_BE_CLEANED_LIST,
            "comparison_level": COMPARISON_LEVEL,
            "del_chunk_size": DEL_CHUNK_SIZE,
            "exclude_mailbox_list": EXCLUDE_MAILBOX_LIST,
            "mailbox_mapping": MAILBOX_MAPPING,
            "single_mailarchive": SINGLE_MAILARCHIVE,
            "no_subfolders": NO_SUBFOLDERS,
            "create_mailboxes": CREATE_MAILBOXES,
            "anonymous_mailbox_names" : ANONYMOUS_MAILBOX_NAMES
        }
    )

    # override specific values if additional config dict is supplied (does  
    # only affect calls from other python modules) - if there is no extra
    # config dict, settings are those from config section.
    if config != {}:
        _CONF.update(config)    

    print("***")
    if _CONF['anonymous_logging']:
        if tool.upper() not in ("LIST", "TEST"):
            print("\nWARNING: [-f] currently only fully "
                  "supported for tools LIST and TEST\n")        

    try:
        logging.info('%s %s started' % (script_name, tool.upper()))
        print("\n")

                ########################################################################
        # tools requiring 1 <source> and 1 <destination> account
        if tool.upper() in ('MAP', 'MIGRATE'):
            # check if there are exactly 2 accounts and exit if there aren't
            if len(account_ids) != 2:
                logging.error('Wrong number of accounts. You need exactly'
                              ' 2 accounts: %s %s '
                              '<SOURCE ACCOUNT> <DESTINATION ACCOUNT>' % (
                                  script_name, tool.upper()))
                sys.exit(2)

            # establish connections to source and destination servers
            if _connect_src_and_dest(account_ids[0], account_ids[1]):
                # start the tool specified
                if tool.upper() == 'MAP':
                    run_map_mailboxes()
                elif tool.upper() == 'MIGRATE':
                    run_migration()

        ########################################################################
        # tools requiring a list of at least 1 IMAP account
        # exceptions handled by argparse (N args +)
        elif tool.upper() in ('LIST', 'CLEAN', 'DEL_EMPTIES'):
            logging.info("The following IMAP servers will be processed: %s" %
                         ", ".join(account_ids))
            print("\n")
            for imap_id in account_ids:
                # establish a connection to specific imap server
                if _connect(imap_id):
                    # start the tool specified
                    if tool.upper() == 'LIST':
                        run_list_mailboxes(imap_id)
                    elif tool.upper() == 'CLEAN':
                        run_de_duplicate(imap_id)
                    elif tool.upper() == 'DEL_EMPTIES':
                        run_delete_empty_mailboxes(imap_id)
                    print("\n")

        elif tool.upper() == 'TEST':
            logging.info("The following IMAP servers will be tested: %s" %
                         ", ".join(account_ids))
            if _CONF['quiet']:
                print("Command line option [-q] is set: No output on console, "
                      "test results will still be accessible in logfile.")
            print("\n")
            for imap_id in account_ids:
                run_test_response(imap_id)
                print("\n")

    except KeyboardInterrupt:
        logging.info('\n%s %s interrupted by user' % (script_name,
                                                      tool.upper()))

    finally:
        disconnect_all()
        print("\n")
        logging.info('%s %s ended' % (script_name, tool.upper()))
        print("***")



def start_and_config_tools_from_commandline(args):

    if args.quiet:
        console.setLevel(logging.CRITICAL)
        _CONF.update({"quiet": True})

        #logfile_handler.setLevel(logging.DEBUG)
    elif args.verbose:
        console.setLevel(logging.DEBUG)
        logfile_handler.setLevel(logging.DEBUG)
        _CONF.update({"quiet": False})
    else:
        console.setLevel(logging.INFO)
        logfile_handler.setLevel(logging.DEBUG)
        _CONF.update({"quiet": False})

    # store relevant commandline arguments in _CONF dictionary
    # accessible for all functions without the need to pass them on
    _CONF.update(
        {
            "called_from": "command_line",
            "tool": args.tool.upper(),
            "all_mailboxes": args.all_mailboxes,
            "duplicate_filter": args.duplicate_filter,
            "expunge_deleted": args.expunge_deleted,
            "dry_run": args.dry_run,
        }
    )

    # override config section values if command line option is used

    # if [-f] is not supplied
    if args.anonymous_logging is None:
        # use value in config section
        _CONF.update({"anonymous_logging": ANONYMOUS_LOGGING})
    else:
        _CONF.update({"anonymous_logging": args.anonymous_logging})


    start_and_config_tools(args.tool.upper(), args.accounts)

def _get_tool_overview():

    DOC_STRINGS_TOOLS = [run_list_mailboxes.__doc__, 
                         run_test_response.__doc__,
                         run_map_mailboxes.__doc__, 
                         run_migration.__doc__, 
                         run_de_duplicate.__doc__, 
                         run_delete_empty_mailboxes.__doc__]

    TOOL_OVERVIEW = "Tools:\n"
    TOOL_OVERVIEW += "{}\n\n".format(6 * "-")
    for tool, docstring in zip(TOOLS, DOC_STRINGS_TOOLS):
        TOOL_OVERVIEW += "**{}**\n".format(tool)
        TOOL_OVERVIEW += "{}\n\n".format(textwrap.indent(docstring, "\t"))

    return TOOL_OVERVIEW    

def _get_rst_title(string, symbol):
    multiplier = len(string)
    string = string + "\n" + multiplier * symbol
    return string

#####################################TIMER######################################
# Timer (class) in combination with 'with' statements, adapted from a blogpost
# by Jeff Prshing (no license specified):
# http://preshing.com/20110924/timing-your-code-using-pythons-with-statement/
class Timer:
    def __enter__(self):
        self.start = time.time()    # use time for cross-platform compatiblity
        #self.start = time.clock()  # works on Windows, might be more accurate
        return self

    def __exit__(self, *args):
        self.end = time.time()      # use time for cross-platform compatiblity
        #self.end = time.clock()    # works on Windows, might be more accurate
        self.interval = self.end - self.start

def log_interval(interval):
    if _CONF['tool'] == 'TEST':
        logger3.info('Request took %.03f sec.' % interval)
    else:
        logger3.debug('Request took %.03f sec.' % interval)


##################################COMMANDLINE###################################
def main():

    parser = argparse.ArgumentParser(
        description=DESCRIPTION,
        #epilog="\n\n",
        formatter_class=argparse.RawDescriptionHelpFormatter)

    group1 = parser.add_mutually_exclusive_group()

    parser.add_argument('tool', metavar='TOOL',
                        choices=TOOLS,
                        help=TOOLS_HELP)

    parser.add_argument('accounts', metavar='ACCOUNT', type=str, nargs='+',
                        choices=[k for k in sorted(IMAP_ACCOUNTS.keys())],
                        help='List of unique IMAP account ids specified in '
                        'config section or optional imap_accounts.py. Tools '
                        'MAP and MIGRATE need exactly two accounts '
                        '*source* -> *destination*, the other tools will '
                        'process one or more accounts in sequence.')

    parser.add_argument('-a', '--all', dest='all_mailboxes',
                        action="store_true", default=False,
                        help='process all mailboxes except "Trash" or empty '
                        'folders (DEFAULT: process only mailboxes '
                        'specified in config section)')

    parser.add_argument('-d', '--duplicate_filter', dest='duplicate_filter',
                        action="store_true", default=False,
                        help='turn on duplicate email filter '
                        'in MIGRATE mode (DEFAULT: OFF)')

    parser.add_argument('-f', '--prepare_forum_post', dest='anonymous_logging',
                        action="store_true", default=None,
                        help='turn on anonymous logging - user data will be '
                        'anonymised in console output with logging level .INFO '
                        '(DEFAULT: OFF) Supported tools: LIST and TEST')    

    parser.add_argument('-e', '--expunge_deleted', dest='expunge_deleted',
                        action="store_true", default=False,
                        help='permanently delete all emails with the flag '
                        '"\deleted" from your IMAP account '
                        'WARNING: THIS ACTION CANNOT BE UNDONE! '
                        '(DEFAULT: OFF)')

    parser.add_argument('-n', '--dry-run',
                        dest='dry_run',
                        action="store_true", default=False,
                        help='perform a trial run without copying or '
                        'deleting any messages'
                        '(DEFAULT: OFF)')


    parser.add_argument('--version', action='version',
                        version='%(prog)s {0}'.format(".".join(VERSION)))

    group1.add_argument('-q', '--quiet', action="store_true", default=False,
                        help='quiet (no logging output on console)')
    group1.add_argument('-v', '--verbose', action="store_true", default=False,
                        help='more output (debug level on console)')

    args = parser.parse_args()

    start_and_config_tools_from_commandline(args)



#############################BASIC INFO & LOGGING###############################

if __name__ == '__main__':

    from __init__ import *

    DOC_STRING_STRIPPED = "\n".join((__doc__.split("\n")[2:]))

    TOOLS = ['LIST', 'TEST', 'MAP', 'MIGRATE', 'CLEAN', 'DEL_EMPTIES']

    TOOLS_HELP = "choose between {} or {} " \
        "(descriptions above)".format(", ".join(TOOLS[:-1]), TOOLS[-1])

    TOOL_OVERVIEW = _get_tool_overview()

    OOB_HINT = """.. HINT::

       Works out of the box for ``GMAIL``, ``OUTLOOK``        
       and ``ICLOUD``, using default options. 

       For other servers and custom options, don't   
       forget to update the **CONFIG SECTION** at the    
       beginning of this script before you run it.   

"""
    CAUTION = """.. CAUTION::

    Test TOOLS using ``[-n] --dry_run`` option,
    before applying any changes to your
    productive IMAP account.

"""

    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)

    # define a Handler which writes DEBUG messages or higher to rotating logfile
    logfile_handler = RotatingFileHandler(LOGFILE,
                                          mode='a',
                                          maxBytes=LOGFILE_MAX_BYTES,
                                          backupCount=LOGFILE_BACKUP_COUNT)
    #logfile_handler.setLevel(logging.DEBUG)        # no effect: level needs
                                                    # to be set on root logger
                                                    # 'rotating_logfile' below


    # set a format which is simpler for console use
    formatter = logging.Formatter(CONSOLE_FMT, datefmt=CONSOLE_DATEFMT)
    logfile_formatter = logging.Formatter(LOGFILE_FMT, datefmt=LOGFILE_DATEFMT)
    # tell the handler to use this format
    console.setFormatter(formatter)
    logfile_handler.setFormatter(logfile_formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    rotating_logfile = logging.getLogger('')        # two steps needed for
    rotating_logfile.addHandler(logfile_handler)    # rotating_handler
    rotating_logfile.setLevel(logging.DEBUG)

    # Loggers representing the 3 main sections of the script
    logger1 = logging.getLogger('server')
    logger2 = logging.getLogger('email')
    logger3 = logging.getLogger('tools')

    script_name = __file__.split(os.path.sep)[-1]  

    try: 
        print("\n")
        CONSOLE_TITLE = "{} v{} © {} {}".format(PROJECT_TITLE, VERSION, 
                                                 COPYRIGHT_YEARS, AUTHOR_INITIALS)
        if os.name == "posix":
            # Write script name to terminal title bar
            # Source: http://code.activestate.com/recipes/578662/
            print("\x1B]0;{}\x07".format(CONSOLE_TITLE))
        elif os.name == "nt":
            try:
                import win32api             
            except ImportError:
                logging.debug("Python3 Module: 'win32api' not installed.")
            console_title = win32api.GetConsoleTitle()
            script_call_info = console_title.split(script_name)[1]
            win32api.SetConsoleTitle("{}: {}".format(CONSOLE_TITLE, script_call_info))               
    except:
        logging.debug("Unable to set Console Title")
        print("\n")



    DESCRIPTION = """
{}
{}

{}

:Version: {}
:Copyright: {} 
:License: {}
:Author: {}

{}

{}

{}

{}

""".format(len(PROJECT_FULL_TITLE) * "=", 
           _get_rst_title(PROJECT_FULL_TITLE, "="), 
           _get_rst_title(PROJECT_SUBTITLE, "-"), VERSION,  
           COPYRIGHT_YEARS, LICENSE, AUTHOR_PLUS_EMAIL, DOC_STRING_STRIPPED, 
           OOB_HINT, TOOL_OVERVIEW, CAUTION)




    main()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# test_imap_server_response.py
# :author: Markus Killer
# :copyright: September 2013
# :license: GNU GPLv3, see LICENSE for more details.
""".. |modtitle| replace:: Test IMAP server response

This simple diagnostic tool lists response times for the most basic
IMAP server interactions (CONNECT, LOGIN, LIST FOLDERS, SELECT FOLDERS).
Makes use of Python's standard library (no additional modules needed).

Tested with Active Python 3.2.2 and 3.3.2 (32-bit) on Windows 8 (64-bit),
Python 3.3.1 on Linux (Lubuntu 13.04 - 64-bit) and
Active Python 3.3.2 on Mac OSX.

No further configuration needed to test OUTLOOK.COM's new IMAP server response.

Usage:

       python3 test_imap_server_response.py

You will be prompted for username and password.
"""
import imaplib
import time
import sys

#: container for IMAP server login details
IMAP_ACCOUNTS = {}

###################TEST IMAP SERVER RESPONSE - CONFIGURATION####################

#: Update your personal account details. If username/password is empty string,
#: script will prompt for missing credentials at runtime (just 1 attempt).
#:
#: It is very easy to add login details of additional IMAP accounts:
#: Just copy and adapt IMAP_ACCOUNTS.update() blocks (9 lines each) below.
#:
#: IMPORTANT: The 2nd line of each block needs to be unique (otherwise earlier
#:            credentials with the same identifier will be overwritten)
#:            Don't forget to inlcude new accounts in the list of
#:            ACCOUNTS_TO_BE_TESTED below.
IMAP_ACCOUNTS.update(
    {"GMAIL":                              # unique account identifier type(str)
                                           # e.g. "GMAIL_XYZ" type(str)
    {'host': 'imap.gmail.com',             # e.g. imap.gmail.com type(str)
     'port': 993,                          # e.g. 993 type(int)
     'username': '',                       # e.g. xyz@gmail.com type(str)
     'password': '',                       # e.g. password_for_gmail / type(str)
     'ssl': True                           # True type(bool)
    }})

IMAP_ACCOUNTS.update(
    {"OUTLOOK":

    {'host': 'imap-mail.outlook.com',
     'port': 993,
     'username': '',
     'password': '',
     'ssl':True
    }})

IMAP_ACCOUNTS.update(
    {"ICLOUD":

    {'host': 'imap.mail.me.com',
     'port': 993,
     'username': '',
     'password': '',
     'ssl':True
    }})

#: Default: Your OUTLOOK.COM account is tested. You may specify as many accounts
#: as you included above. The script will work through this list of account
#: identifiers (see e.g. line 36) in specified order).
#: Example: ACCOUNTS_TO_BE_TESTED = ['GMAIL', 'ICLOUD', 'OUTLOOK']
ACCOUNTS_TO_BE_TESTED = ['OUTLOOK']

#####################NO CHANGES NECESSARY BEYOND THIS POINT#####################

#####################################TIMER######################################
# Timer adapted from:
# http://preshing.com/20110924/timing-your-code-using-pythons-with-statement/
class Timer:
    def __enter__(self):
        self.start = time.time()    # use time for cross-platform compatiblity
        #self.start = time.clock()  # works on Windows, might be more accurate
        return self

    def __exit__(self, *args):
        self.end = time.time()      # use time for cross-platform compatiblity
        #self.end = time.clock()    # works on Windows, might be more accurate
        self.interval = self.end - self.start

def print_interval(interval):
    print('Request took %.03f sec.' % interval)

##############################TEST SERVER RESPONSE##############################
def test_server_response(account):
    try:
        print("Connecting to %s:" % account['host'])
        connection = time_server_connect(account)
        print("Connection state: ", connection.state)

        print("Authenticating at %s:" % account['host'])
        connection, response = time_server_login(connection, account)
        print("Connection state: ", connection.state)
        print("Server response: ", response)

        if connection.state == "AUTH":
            print("Retrieving folder list ...")
            response = time_list_folders(connection)
            folder_list = [f.decode('utf-8') for f in response[1]]
            folder_list = [f.split('"/"')[1].strip() for f in folder_list \
                           if "Noselect" not in f.split('"/"')[0]]
            print("\nThere are {} folders for this user on {}.".format(
                  len(folder_list), connection.host))
            print("\nTiming SELECT command for all folders ...\n\n")
            folder_counter = 0
            for folder in folder_list:
                folder_counter += 1
                print("SELECT", folder, "({} of {})".format(folder_counter,
                                                            len(folder_list)))
                try:
                    response = time_select_folder(connection, folder)
                    if "error" in response[1][0].decode('utf-8'):
                        print(response, end="\n\n", file=sys.stderr)
                    else:
                        print("There are {} messages in {}\n".format(
                              int(response[1][0]), folder))
                        connection.close()
                except Exception as e:
                    print(e, file=sys.stderr)
    finally:
        try:
            logout = connection.logout()
            print("\nLogging out of %s:" % connection.host, logout)
        except:
            print("Script terminated before connection to IMAP Account "
                  "could be established.", file=sys.stderr)


def time_server_connect(account):
    try:
        with Timer() as t:
            if account['ssl']:
                _conn = imaplib.IMAP4_SSL(account['host'], account['port'])
            else:
                _conn = imaplib.IMAP4(account['host'], account['port'])
    except Exception as e:
        print(e, file=sys.stderr)
    finally:
        print_interval(t.interval)
        return _conn

def time_server_login(connection, account):
    username, password = _get_credentials(account)
    try:
        with Timer() as t:
            response = connection.login(username, password)
    except Exception as e:
        response = e
    finally:
        print_interval(t.interval)
        return connection, response

def time_list_folders(connection):
    try:
        with Timer() as t:
            response = connection.list()
    except Exception as e:
        response = []
        print(e, file=sys.stderr)
    finally:
        print_interval(t.interval)
        return response

def time_select_folder(connection, folder):
    try:
        with Timer() as t:
            response = connection.select(folder)
    except Exception as e:
        response = connection.state
        print(e, file=sys.stderr)
    finally:
        print_interval(t.interval)
        return response

def _get_credentials(account):
    if account['username'] == '':
        username = input('Enter username for this IMAP account --> ')
    else:
        username = account['username']
    if account['password'] == '':
        import getpass
        password = getpass.getpass()
    else:
        password = account['password']
    return username, password

if __name__ == "__main__":

    #: work through list of accounts specified in config section
    for account in ACCOUNTS_TO_BE_TESTED:
        print(account)
        test_server_response(IMAP_ACCOUNTS[account])
        print("\n\n\n")